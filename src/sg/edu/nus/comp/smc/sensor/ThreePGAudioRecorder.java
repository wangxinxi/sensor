package sg.edu.nus.comp.smc.sensor;

import java.io.File;
import java.io.IOException;

import android.media.MediaRecorder;
import android.os.Environment;

public class ThreePGAudioRecorder implements AudioRecorder {
	private MediaRecorder recorder;
	private static final String TAG = "Sensor";
    private static final String AUDIO_RECORDER_FILE_EXT_3PG = ".3gp";
    private static final String AUDIO_RECORDER_FOLDER = "audio";
    private String audioFilename = null;
    
	public ThreePGAudioRecorder() {
		
	}
	
	
	public String getFilePath() {
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File file = new File(filepath, AUDIO_RECORDER_FOLDER);
		
		if (!file.exists()) {
			file.mkdirs();
		}
		
		return (file.getAbsolutePath() + "/" + audioFilename + AUDIO_RECORDER_FILE_EXT_3PG);
	}
	
	
	public void startRecording(String filename) {
		audioFilename = filename;
		
		// set up the audio recorder
	    recorder = new MediaRecorder();

	    recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
	    recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
	    recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_WB);
	    recorder.setOutputFile(getFilePath());

	    recorder.setOnErrorListener(new MediaRecorder.OnErrorListener() {
	    	public void onError(MediaRecorder mr, int what, int extra) {
	    		
	    	}
	    });
	    
	    recorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {

			public void onInfo(MediaRecorder mr, int what, int extra) {
			}
	    	
	    });

	    try {
	    	recorder.prepare();
	    	recorder.start();
	    } catch (IllegalStateException e) {
	    	e.printStackTrace();
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	}
	
	
	public void stopRecording() {
		recorder.stop();
		recorder.reset();
		recorder.release();

		recorder = null;
	}
}

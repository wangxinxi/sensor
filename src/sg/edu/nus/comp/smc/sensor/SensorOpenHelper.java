package sg.edu.nus.comp.smc.sensor;

import java.io.File;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

public class SensorOpenHelper { // extends SQLiteOpenHelper {
	private static final String TAG = "Sensor";
	
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "sensor.db";
	public static final String ACCEL_TABLE_NAME = "accelerometer";
	public static final String GYRO_TABLE_NAME = "gyroscope";
	public static final String LIGHT_TABLE_NAME = "light";
	public static final String LOCATION_TABLE_NAME = "location";
	public static final String SOUND_TABLE_NAME = "sound";
	public static final String ANNOTATION_TABLE_NAME = "annotation";
	public static final String BATTERY_TABLE_NAME = "battery";
	public static final String PHONE_INFO_TABLE_NAME = "phone_info";
	public static final String SENSOR_INFO_TABLE_NAME = "sensor_info";
	public static final String CONFIGURATION_TABLE_NAME = "configuration";
	
	private SQLiteDatabase db = null;
	
	public SensorOpenHelper(Context context) {
		// super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
		
		try {
			File sdcard = Environment.getExternalStorageDirectory();
			String dbfile = sdcard.getAbsolutePath() + File.separator+ DATABASE_NAME ;
			
			db = SQLiteDatabase.openOrCreateDatabase(dbfile, null);
		} catch (SQLException e) {
			Log.d(TAG, e.toString());
		}
		onCreate(db);
	}

	// @Override
	public void onCreate(SQLiteDatabase db) {
		try {
		// TODO Auto-generated method stub
			db.execSQL("CREATE TABLE IF NOT EXISTS " + CONFIGURATION_TABLE_NAME + " "
					+ "(key TEXT, value TEXT)");
			
			db.execSQL("CREATE TABLE IF NOT EXISTS " + ACCEL_TABLE_NAME + " "
					+ "(annotation_id INT, timestamp INT, x FLOAT, y FLOAT, z FLOAT)");
			
			db.execSQL("CREATE TABLE IF NOT EXISTS " + ACCEL_TABLE_NAME + " "
					+ "(annotation_id INT, timestamp INT, x FLOAT, y FLOAT, z FLOAT)");

			db.execSQL("CREATE TABLE IF NOT EXISTS " + GYRO_TABLE_NAME + " "
					+ "(annotation_id INT, timestamp INT, x FLOAT, y FLOAT, z FLOAT)");

			db.execSQL("CREATE TABLE IF NOT EXISTS " + LIGHT_TABLE_NAME + " "
					+ "(annotation_id INT, timestamp INT, light INT)");

			db.execSQL("CREATE TABLE IF NOT EXISTS " + LOCATION_TABLE_NAME + " "
					+ "(annotation_id INT, timestamp INT, latitude DOUBLE, longitude DOUBLE, "
					+ "altitude DOUBLE, bearing DOUBLE, speed DOUBLE, accuracy DOUBLE, provider TEXT)");

			db.execSQL("CREATE TABLE IF NOT EXISTS " + SOUND_TABLE_NAME + " "
					+ "(annotation_id INT, timestamp INT, noisy FLOAT)");
			
			db.execSQL("CREATE TABLE IF NOT EXISTS " + ANNOTATION_TABLE_NAME + " "
					+ "(_id INTEGER PRIMARY KEY AUTOINCREMENT, location TEXT, activity TEXT, " 
					+ " start_timestamp INT, end_timestamp INT, deleted BOOLEAN DEFAULT 0)" );
			
			db.execSQL("CREATE TABLE IF NOT EXISTS " + BATTERY_TABLE_NAME + " "
					+ "(annotation_id INT, timestamp INT, scale INT, level INT, voltage INT, temp INT)");
			
			db.execSQL("CREATE TABLE IF NOT EXISTS " + PHONE_INFO_TABLE_NAME + " "
					+ "(key TEXT PRIMARY KEY, value TEXT)");

			db.execSQL("CREATE TABLE IF NOT EXISTS " + SENSOR_INFO_TABLE_NAME + " "
					+ "(type INT, vendor TEXT, version INT, resolution REAL, power REAL," +
					" name TEXT, minDelay INT, maximumRange FLOAT, PRIMARY KEY(type, vendor, version, name))");
			
			db.execSQL("CREATE INDEX IF NOT EXISTS anno_id_idx ON annotation (_id)");
			db.execSQL("CREATE INDEX IF NOT EXISTS anno_deleted_idx ON annotation (deleted)");
			db.execSQL("CREATE INDEX IF NOT EXISTS acc_id_idx ON accelerometer (annotation_id)");
			db.execSQL("CREATE INDEX IF NOT EXISTS gyro_id_idx ON gyroscope (annotation_id)");
			db.execSQL("CREATE INDEX IF NOT EXISTS loc_id_idx ON location (annotation_id)");

		} catch (SQLException e) {
			Log.d(TAG, e.getMessage());
		}
	}
	
//	public int confInt(String key) {
//		Cursor c = db.rawQuery("SELECT value FROM " + CONFIGURATION_TABLE_NAME + " WHERE key == \"" + key + "\"", null);
//		if(c.moveToFirst()) {
//			CONFIGURATION_TABLE_NAME
//		}
//	}
//	
//	public String getConfString(String key) {
//		db.rawQuery("SELECT value FROM " + CONFIGURATION_TABLE_NAME + " WHERE key == \"" + key + "\"", null);
//	}

	// @Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	
	public SQLiteDatabase getWritableDatabase() {
		return db;
	}
}

package sg.edu.nus.comp.smc.sensor;

public interface AudioRecorder {
	void startRecording(String filename);
	void stopRecording();
	String getFilePath();
}

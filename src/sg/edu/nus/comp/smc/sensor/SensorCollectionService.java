package sg.edu.nus.comp.smc.sensor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationListener;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.util.Log;
import sg.edu.nus.comp.smc.sensor.SensorService;
import sg.edu.nus.comp.smc.sensor.SensorOpenHelper;

public class SensorCollectionService extends Service implements SensorEventListener {
	private final String TAG = "Sensor";
	
	private SensorManager sensorManager = null;
	private SensorOpenHelper openHelper = null;
	private SQLiteDatabase database = null;
	
	private LocationManager lm = null;
	private LocationListener locationListener = null;
	private AudioRecorder audioRecorder = null;
	
	Sensor gyroSensor = null;
	Sensor accelSensor = null;
	Sensor lightSensor = null;
	
	private int gyroCount = 0;
	private int accelCount = 0;
	private int lightCount = 0;
	private int gpsCount = 0;
	
	private long gyroTimestamp = 0;
	private long accelTimestamp = 0;
	private long lightTimestamp = 0;
	private long startTimestamp = 0;
	
	private String location = null;
	private String activity = null;
	
	private boolean started = false;
	private long annotationId = 0;
	
	private Thread recordingThread = null;
	private Looper threadLooper;	
	
	@Override
	public IBinder onBind(Intent intent) {
    	Log.d(TAG, "sensor recoding service");

		createDatabase();
		
		return myRemoteServiceStub;
	}
	
	private void createDatabase()
	{
		if(openHelper == null)
			openHelper = new SensorOpenHelper(getApplicationContext());
		
		if(database == null)
			database = openHelper.getWritableDatabase();	  
		
		database.setLockingEnabled(false);
	    
	    Log.d(TAG, getClass().getSimpleName() + " onCreate()" + database.getPath());
	}
	
	private SensorService.Stub myRemoteServiceStub = new SensorService.Stub() {

		public void startRecording(String loc, String act)
				throws RemoteException {
		    location = loc;
		    activity = act;
		    threadLooper = null;
		    
		    // start the recording thread
			recordingThread = new Thread(new Runnable() {

				public void run() {
					recordData();
				}

			}, "Sensor Recording Thread");

			recordingThread.start();
		    
		}

		public void stopRecording() throws RemoteException {
			// TODO Auto-generated method stub
			threadLooper.quit();
		}

		public String getLocation() throws RemoteException {
			return location;
		}

		public String getActivity() throws RemoteException {
			return activity;
		}

		public boolean started() throws RemoteException {
			return started;
		}

		public int getGyroCount() throws RemoteException {
			return gyroCount;
		}

		public int getAccelCount() throws RemoteException {
			return accelCount;
		}

		public int getLightCount() throws RemoteException {
			return lightCount;
		}

		public int getGPSCount() throws RemoteException {
			return gpsCount;
		}
		
		public void getRecordedSessions(List<String> sessions, List<String> sessionIds) {
//			Cursor c = database.rawQuery(
//					"SELECT _id, location, activity, start_timestamp, end_timestamp, acc_count,  COUNT(timestamp) loc_count FROM " + 
//					 "(SELECT _id, location, activity, start_timestamp, end_timestamp, COUNT(timestamp) acc_count FROM annotation LEFT OUTER JOIN accelerometer ON _id == annotation_id WHERE deleted == 0 GROUP BY _id) " + 
//					 " LEFT OUTER JOIN location ON _id == annotation_id GROUP BY _id", null);	

			Cursor c = database.rawQuery("SELECT * FROM annotation WHERE deleted == 0", null);
			DateFormat formatter = new SimpleDateFormat("dd/MM HH:mm:ss");

			// long startTime = System.currentTimeMillis();
			while (c.moveToNext()) {
				// Log.d(TAG, "Time elapsed before gets: " + (System.currentTimeMillis() - startTime));
				long id = c.getLong(0);
				String location = c.getString(1);
				String activity = c.getString(2);
				long startTimestamp = c.getLong(3);
				long endTimestamp = c.getLong(4);
				
				// this recording is still continuing 
				if(endTimestamp == 0)
					continue;
				
				Cursor c1 = database.rawQuery(
						"SELECT COUNT(*) FROM location WHERE annotation_id == "
								+ id, null);
				int locationSamples = 0; // c.getInt(6);
				if (c1.moveToNext())
					locationSamples = c1.getInt(0);

				Cursor c2 = database.rawQuery(
						"SELECT COUNT(*) FROM accelerometer WHERE annotation_id == "
								+ id, null);
				int accelSamples = 0; // c.getInt(5);
				if (c2.moveToNext())
					accelSamples = c2.getInt(0);
				
				Calendar start = Calendar.getInstance();
				start.setTimeInMillis(startTimestamp);

				Calendar end = Calendar.getInstance();
				end.setTimeInMillis(endTimestamp);

				// Log.d(TAG, "Time elapsed before add: " + (System.currentTimeMillis() - startTime));
				sessions.add(activity + "@" + location + " "
						+ formatter.format(start.getTime()) + " "
						+ formatter.format(end.getTime()) + " GPS: "
						+ locationSamples + " acc: " + accelSamples);
				
				sessionIds.add(Long.toString(id));
				// Log.d(TAG, "Time elapsed after add: " + (System.currentTimeMillis() - startTime));
			}
		}

		public void deleteRecordedSession(String id) throws RemoteException {
			database.execSQL("UPDATE annotation SET deleted = 1 WHERE _id == " + id);
		}
	};

	
	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, getClass().getSimpleName() + " onCreate()");
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, getClass().getSimpleName()+ " onDestroy()");
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		Log.d(TAG, getClass().getSimpleName()+ " onStart()");
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		Log.d(TAG, sensor.getName() + " sensor accuracy changed " + accuracy);
	}

	
	public void initializeSensorsAndDatabase(Handler handler, Looper looper) {
		createDatabase();
		
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		if(sensorManager == null) {
			Log.e(TAG, "cannot get SensorManager");
		}
	    
		// gyroscope
	    gyroSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
	    if(gyroSensor != null)
			sensorManager.registerListener(SensorCollectionService.this,
					gyroSensor, SensorManager.SENSOR_DELAY_FASTEST, handler);
	    else
	    	Log.e(TAG, "gyroscope cannot be initialized");
	    
	    // accelerometer
	    accelSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    if(accelSensor != null)
			sensorManager.registerListener(SensorCollectionService.this,
					accelSensor, SensorManager.SENSOR_DELAY_FASTEST, handler);
	    else
	    	Log.e(TAG, "accelerometer cannot be initialized");
	    
	    // light
	    lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
	    if(lightSensor != null)
			sensorManager.registerListener(SensorCollectionService.this,
					lightSensor, SensorManager.SENSOR_DELAY_FASTEST, handler);
	    else 
	    	Log.e(TAG, "light sensor cannot be initialized");
	    
		// location
	    lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
	    if(lm != null) {
	    	locationListener = new MyLocationListener();
	    	lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener, looper);
	    	lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener, looper);
	    } else {
	    	Log.e(TAG, "location service can	not be initialized");
	    }
	    
//	    // battery
//	    batteryReceiver = new BatteryBroadcastReceiver();
//	    IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
//	    registerReceiver(batteryReceiver, filter);
	    
	    started = true;
	    gyroTimestamp = 0;
	    accelTimestamp = 0;
	    lightTimestamp = 0;

	    accelCount = 0;
	    gyroCount = 0;
	    lightCount = 0;
	    gpsCount = 0;
		database.beginTransaction();
		
	    ContentValues contentValues = new ContentValues();
	    contentValues.put("location", location);
	    contentValues.put("activity", activity);
	    startTimestamp = System.currentTimeMillis();
	    contentValues.put("start_timestamp", startTimestamp);
	    annotationId = database.insert(SensorOpenHelper.ANNOTATION_TABLE_NAME, null, contentValues);
	   
	    
	    // audio recording
	    if(audioRecorder == null)
	    	audioRecorder = new WavAudioRecorder();
	    audioRecorder.startRecording(Long.toString(annotationId));
	    
	    
	    // basic information about the phone
	    phoneInfo();
	    
	    Log.d(TAG, "Recording started: location " + location + " activity " + activity);	
	}
	
	
	public void finalizeDatabaseAndSensors() {
		started = false;
		
		ContentValues contentValues = new ContentValues();
		contentValues.put("end_timestamp", System.currentTimeMillis());
		database.update(SensorOpenHelper.ANNOTATION_TABLE_NAME,
				contentValues, "_id=" + annotationId, null);

	    database.setTransactionSuccessful();
	    database.endTransaction();
	    
	    if(lm != null)
	    	lm.removeUpdates(locationListener);
	    
	    // unregisterReceiver(batteryReceiver);
	    
		if (sensorManager != null) {
			sensorManager.unregisterListener(SensorCollectionService.this);
			
			if (gyroSensor != null)
				sensorManager.unregisterListener(
						SensorCollectionService.this, gyroSensor);
			
			if (accelSensor != null)
				sensorManager.unregisterListener(
						SensorCollectionService.this, accelSensor);
			
			if (lightSensor != null)
				sensorManager.unregisterListener(
						SensorCollectionService.this, lightSensor);
		}
		
		audioRecorder.stopRecording();
	}
	
	public void recordData() {
		Looper.prepare();
		
		threadLooper = Looper.myLooper();
		initializeSensorsAndDatabase(new Handler(), threadLooper);		
		
		Looper.loop();
		
		Log.d(TAG, "loop quited");
		finalizeDatabaseAndSensors();
	}
	
	public void onSensorChanged(SensorEvent sensorEvent) {
		if(System.currentTimeMillis() - startTimestamp > 3600*1000) {
			threadLooper.quit();
		}
		
		if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			if(accelTimestamp == 0) {
				accelTimestamp = sensorEvent.timestamp;
			}			
			
			ContentValues contentValues = new ContentValues();
			contentValues.put("annotation_id", annotationId);
			contentValues.put("timestamp", sensorEvent.timestamp - accelTimestamp);
			contentValues.put("x", sensorEvent.values[0]);
			contentValues.put("y", sensorEvent.values[1]);
			contentValues.put("z", sensorEvent.values[2]);
			
			database.insert(SensorOpenHelper.ACCEL_TABLE_NAME, null, contentValues);
			accelCount++;
			
			if(accelCount % 1000 == 0)
				Log.d(TAG, "accelCount % 1000 == 0");
		} 
		
		else if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
			if(gyroTimestamp == 0)
				gyroTimestamp = sensorEvent.timestamp;
			
			ContentValues contentValues = new ContentValues();
			contentValues.put("annotation_id", annotationId);
			contentValues.put("timestamp", sensorEvent.timestamp - gyroTimestamp);
			contentValues.put("x", sensorEvent.values[0]);
			contentValues.put("y", sensorEvent.values[1]);
			contentValues.put("z", sensorEvent.values[2]);			
			database.insert(SensorOpenHelper.GYRO_TABLE_NAME, null, contentValues);
			
			gyroCount++;
		}
		
		else if (sensorEvent.sensor.getType() == Sensor.TYPE_LIGHT) {
			if(lightTimestamp == 0)
				lightTimestamp = sensorEvent.timestamp;
			
			ContentValues contentValues = new ContentValues();
			contentValues.put("annotation_id", annotationId);
			contentValues.put("timestamp", sensorEvent.timestamp - lightTimestamp);
			contentValues.put("light", sensorEvent.values[0]);
			database.insert(SensorOpenHelper.LIGHT_TABLE_NAME, null, contentValues);
			
			lightCount++;
		}
	}
	
	private void phoneInfo() {

		insertOnePhoneInfo("BOARD", android.os.Build.MODEL);
		insertOnePhoneInfo("BOOTLOADER", android.os.Build.BOOTLOADER);
		insertOnePhoneInfo("CPU_ABI", android.os.Build.CPU_ABI);
		insertOnePhoneInfo("CPU_ABI2", android.os.Build.CPU_ABI2);
		insertOnePhoneInfo("DEVICE", android.os.Build.DEVICE);
		insertOnePhoneInfo("DISPLAY", android.os.Build.DISPLAY);
		insertOnePhoneInfo("FINGERPRINT", android.os.Build.FINGERPRINT);
		insertOnePhoneInfo("HARDWARE", android.os.Build.HARDWARE);
		insertOnePhoneInfo("HOST", android.os.Build.HOST);
		insertOnePhoneInfo("MANUFACTURER", android.os.Build.MANUFACTURER);
		insertOnePhoneInfo("MODEL", android.os.Build.MODEL);
		insertOnePhoneInfo("PRODUCT", android.os.Build.PRODUCT);
		insertOnePhoneInfo("RADIO", android.os.Build.RADIO);
		insertOnePhoneInfo("SERIAL", android.os.Build.SERIAL);
		insertOnePhoneInfo("TAGS", android.os.Build.TAGS);
		insertOnePhoneInfo("USER", android.os.Build.USER);
		
		insertOnePhoneInfo("VERSION.CODENAME", android.os.Build.VERSION.CODENAME);
		insertOnePhoneInfo("VERSION.INCREMENTAL", android.os.Build.VERSION.INCREMENTAL);
		insertOnePhoneInfo("VERSION.RELEASE", android.os.Build.VERSION.RELEASE);
		insertOnePhoneInfo("VERSION.SDK_INT", String.valueOf(android.os.Build.VERSION.SDK_INT));
		
		TelephonyManager telManager =(TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
		insertOnePhoneInfo("SIM_COUNTRY_ISO", telManager.getSimCountryIso());
		insertOnePhoneInfo("LINE1_NUMBER", telManager.getLine1Number());
		
		// get sensors info
		List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
		for(int i = 0; i < sensors.size(); i++) {
			Sensor sensor = sensors.get(i);
			ContentValues contentValues = new ContentValues();
			contentValues.put("type", sensor.getType());
			contentValues.put("vendor", sensor.getVendor());
			contentValues.put("version", sensor.getVersion());
			contentValues.put("resolution", sensor.getResolution());
			contentValues.put("power", sensor.getPower());
			contentValues.put("name", sensor.getName());
			contentValues.put("minDelay", sensor.getMinDelay());
			contentValues.put("maximumRange", sensor.getMaximumRange());
			
			database.insertWithOnConflict(SensorOpenHelper.SENSOR_INFO_TABLE_NAME, null, 
					contentValues, SQLiteDatabase.CONFLICT_IGNORE);	
		}
	}
	
	private void insertOnePhoneInfo(String key, String value) {
		Log.d(TAG, key + " " + value); 
		ContentValues contentValues = new ContentValues();
		contentValues.put("key", key);
		contentValues.put("value", value);
		database.insertWithOnConflict(SensorOpenHelper.PHONE_INFO_TABLE_NAME, null, 
				contentValues, SQLiteDatabase.CONFLICT_IGNORE);		
	}
	
	class MyLocationListener implements LocationListener {

		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			Log.d(TAG, "latitude: " + location.getLatitude() + " Longitude: " 
			+ location.getLongitude() + " Accuracy: " + location.getAccuracy()
			+ "Speed: " + location.getSpeed() + " Provider: " + location.getProvider());
			
			ContentValues contentValues = new ContentValues();
			contentValues.put("annotation_id", annotationId);
			contentValues.put("timestamp", System.currentTimeMillis());
			contentValues.put("latitude", location.getLatitude());
			contentValues.put("longitude", location.getLongitude());
			contentValues.put("altitude", location.getAltitude());
			contentValues.put("bearing", location.getBearing());
			contentValues.put("speed", location.getSpeed());
			contentValues.put("accuracy", location.getAccuracy());
			contentValues.put("provider", location.getProvider());
			database.insert(SensorOpenHelper.LOCATION_TABLE_NAME, null, contentValues);
			
			gpsCount++;
		}

		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}		
	}
	
	class BatteryBroadcastReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			ContentValues contentValues = new ContentValues();
			contentValues.put("annotation_id", annotationId);
			contentValues.put("timestamp", System.currentTimeMillis());
			contentValues.put("scale", intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1));
			contentValues.put("level", intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1));
			contentValues.put("temp", intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1));
			contentValues.put("voltage", intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1));
			database.insert(SensorOpenHelper.BATTERY_TABLE_NAME, null, contentValues);
		}
	}
}

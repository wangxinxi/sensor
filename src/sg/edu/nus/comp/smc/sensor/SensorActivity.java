package sg.edu.nus.comp.smc.sensor;

//import java.util.Iterator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.*;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import sg.edu.nus.comp.smc.sensor.R;
import sg.edu.nus.comp.smc.sensor.SensorService;

public class SensorActivity extends ListActivity {
	private final String TAG = "Sensor";
	
	private RemoteServiceConnection conn = null;
	private SensorService remoteService = null;
	
	private Button annotationControl = null;
	private Spinner locationSpinner = null;
	private Spinner activitySpinner = null;
	
	private Handler handler = new Handler();	
	private TextView accelSamples = null;
	private TextView gpsSamples = null;
	
	private ArrayList<String> sessionInfo;
	private ArrayList<String> sessionIds;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // setup the ui
        setContentView(R.layout.main);
        
        registerForContextMenu(getListView());
        
        locationSpinner = (Spinner) findViewById(R.id.locationSpinner);
        
        ArrayAdapter<CharSequence> locationAdapter = ArrayAdapter.createFromResource(
                this, R.array.locations, android.R.layout.simple_spinner_item);
        locationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locationSpinner.setAdapter(locationAdapter);
        
        activitySpinner = (Spinner) findViewById(R.id.activitySpinner);
        ArrayAdapter<CharSequence> activityAdapter = ArrayAdapter.createFromResource(
                this, R.array.activities, android.R.layout.simple_spinner_item);
        activityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activitySpinner.setAdapter(activityAdapter);
        
        accelSamples = (TextView) findViewById(R.id.accel_samples);
        gpsSamples = (TextView) findViewById(R.id.gps_samples);
        
        annotationControl = (Button) findViewById(R.id.annotationControl);
        annotationControl.setOnClickListener(new OnClickListener () {

			public void onClick(View v) {
				try {					
			        if(!hasStorage(true)) {
			        	Toast.makeText(getApplicationContext(), "No writeable external storage found", Toast.LENGTH_LONG);
			        	return;
			        }			        
			        			        
			        
					if (annotationControl.getText().equals(getResources().getText(R.string.start))) {
						annotationControl.setText(getResources().getText(R.string.stop));

						String location = (String) locationSpinner
								.getSelectedItem();
						String activity = (String) activitySpinner
								.getSelectedItem();

						remoteService.startRecording(location, activity);

					} else {
				        AlertDialog stopAlert = new AlertDialog.Builder(SensorActivity.this).create();
				        stopAlert.setTitle("Stop Recording");
				        stopAlert.setMessage("Are you sure you want to stop the recording?");
				        stopAlert.setButton("Yes", new AlertDialog.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								try {
									annotationControl.setText(getResources().getText(R.string.start));
									remoteService.stopRecording();
								} catch (RemoteException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}						
								showRecordedSessions();
							}
				        	
				        });
				        
				        stopAlert.setButton2("No", new AlertDialog.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
							}
				        });
				        stopAlert.show();
					}					
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
        	
        });        
        
        
        if(!hasStorage(true)) {
        	Toast.makeText(getApplicationContext(), "No writeable external storage found", Toast.LENGTH_LONG);
        }
        
        // start and bind the service and update the interface 
        startAndBindService();
		
        handler.postDelayed(new Runnable() {
        	public void run() {
        		if(remoteService != null) {
        			updateInterface();
        		}
        		handler.postDelayed(this, 1000);
        	}
        }, 100);
        
//        Button stopButton = (Button) findViewById(R.id.stopButton);
//        stopButton.setOnClickListener(new OnClickListener () {
//			public void onClick(View v) {
//				Log.d(TAG, "stop service");
//				
//        		Intent i = new Intent();
//        		i.setClassName("sg.edu.nus.comp.smc", "sg.edu.nus.comp.smc.SensorCollectionService");
//        		if(getApplicationContext().stopService(i)) {
//        			Log.d(TAG, "cannot stop service");
//        		}
//        		
//        		if(conn != null) {
//        			Log.d(TAG, "unbind service");
//        			getApplicationContext().unbindService(conn);
//        		}
//        		conn = null;
//			}        	
//        });
        
        
//        Button invokeButton = (Button) findViewById(R.id.invokeButton);
//        invokeButton.setOnClickListener(new OnClickListener () {
//
//			public void onClick(View v) {
//				try {
//					if(remoteService != null) {
//						int accelCount = remoteService.getAccelCount();
//						int gyroCount = remoteService.getGyroCount();
//						int lightCount = remoteService.getLightCount();
//
//						accelText.setText(Integer.toString(accelCount));
//						gyroText.setText(Integer.toString(gyroCount));
//						lightText.setText(Integer.toString(lightCount));
//					}
//				} catch (RemoteException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//        	
//        });
//        
    }
    
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
        ContextMenuInfo menuInfo) {
      if (v.getId() == android.R.id.list) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        
        menu.setHeaderTitle(sessionInfo.get(info.position));
        String[] menuItems = getResources().getStringArray(R.array.menu);
        for (int i = 0; i<menuItems.length; i++) {
          menu.add(Menu.NONE, i, i, menuItems[i]);
        }
      }
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
      AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
      String id = sessionIds.get(info.position);
      
      // Log.d(TAG, "DELETE " + id);
      if(remoteService != null)
		try {
			remoteService.deleteRecordedSession(id);
			showRecordedSessions();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      return true;
    }    
    
    private void showRecordedSessions() {
    	if(remoteService != null) {
    		@SuppressWarnings("unchecked")
			AsyncTask<Void, Void, Void> t = new AsyncTask<Void, Void, Void>() {
    			
				@Override
				protected Void doInBackground(Void... params) {
	        		sessionInfo = new ArrayList<String>();
	        		sessionIds = new ArrayList<String>();
	        		try {
	    				remoteService.getRecordedSessions(sessionInfo, sessionIds);
	    			} catch (RemoteException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}
	        		
					return null;
				}
				
				@Override
			     protected void onPostExecute(Void a) {
			    	 SensorActivity.this.setListAdapter(
			    			 new ArrayAdapter<String>(SensorActivity.this,
			    					 R.layout.simplerow, sessionInfo));
			     }
    		};
    		
    		t.execute();
    	}
    }
    
    private void startAndBindService()
    {
		Log.d(TAG, "start service");
		
		Intent i = new Intent();
		i.setClassName("sg.edu.nus.comp.smc.sensor", "sg.edu.nus.comp.smc.sensor.SensorCollectionService");
		
		conn = new RemoteServiceConnection();
		
		getApplicationContext().bindService(i, conn, Context.BIND_AUTO_CREATE);
    }
    
    
    private void updateInterface() 
    {
    	try {
			if (remoteService != null) {
				// showRecordedSessions();
				boolean started = remoteService.started();
				if (started) {
					String location = remoteService.getLocation();
					for (int i = 0; i < locationSpinner.getCount(); i++) {
						if (locationSpinner.getItemAtPosition(i).equals(
								location)) {
							locationSpinner.setSelection(i);
						}
					}

					String activity = remoteService.getActivity();
					for (int i = 0; i < activitySpinner.getCount(); i++) {
						if (activitySpinner.getItemAtPosition(i).equals(
								activity)) {
							activitySpinner.setSelection(i);
						}
					}
					annotationControl.setText(getResources().getText(R.string.stop));
				}
				else
					annotationControl.setText(getResources().getText(R.string.start));
				
				accelSamples.setText(" " + Integer.toString(remoteService.getAccelCount()));
				gpsSamples.setText(" " + Integer.toString(remoteService.getGPSCount()));
				
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }


    public boolean checkFsWritable() {
        // Create a temporary file to see whether a volume is really writeable.
        // It's important not to put it in the root directory which may have a
        // limit on the number of files.
        String directoryName =
                Environment.getExternalStorageDirectory().toString() + "/audio";
        File directory = new File(directoryName);
        if (!directory.isDirectory()) {
            if (!directory.mkdirs()) {
                return false;
            }
        }
        File f = new File(directoryName, ".probe");
        try {
            // Remove stale file if any
            if (f.exists()) {
                f.delete();
            }
            if (!f.createNewFile()) {
                return false;
            }
            f.delete();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }    
    
    private boolean hasStorage(boolean requireWriteAccess) {
        //TODO: After fix the bug,  add "if (VERBOSE)" before logging errors.
        String state = Environment.getExternalStorageState();
        Log.v(TAG, "storage state is " + state);

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            if (requireWriteAccess) {
                boolean writable = checkFsWritable();
                Log.v(TAG, "storage writable is " + writable);
                return writable;
            } else {
                return true;
            }
        } else if (!requireWriteAccess && Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
    
    
    private class RemoteServiceConnection implements ServiceConnection {

		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "service connected");
			remoteService = SensorService.Stub.asInterface(service);
			showRecordedSessions();
		}

		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "service disconnected");
			remoteService = null;
		}
    }
    
    
	@Override
	protected void onResume() {
		super.onResume();
		
		Log.d(TAG, "resumed ");
	}

	
	@Override
	protected void onStop() {
		Log.d(TAG, "stopped ");
		super.onStop();
	}
	
	@Override
	protected void onDestroy() {
		Log.d(TAG, "exited ");
		super.onStop();
	}
}
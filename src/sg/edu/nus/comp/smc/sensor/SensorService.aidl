package sg.edu.nus.comp.smc.sensor;

interface SensorService 
{	
	void startRecording(String location, String activity);
	void stopRecording();
	
	String getLocation();
	String getActivity();
	
	boolean started();
	
	int getGyroCount();
	int getAccelCount();
	int getLightCount();
	int getGPSCount();	
	
	void getRecordedSessions(out List<String> sessionInfo, out List<String> sessionIds);	
	void deleteRecordedSession(String id);
}
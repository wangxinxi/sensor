package sg.edu.nus.comp.smc.sensor;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

public class NoiseLevelRecorder implements AudioRecorder {
	private static final String TAG = "Sensor"; 
    private static final int RECORDER_SAMPLERATE = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_CONFIGURATION_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    
	private Thread recordingThread = null;
	private int bufferSize; //AudioRecord.getMinBufferSize(samplingRate, channelConfig, audioFormat);
	private AudioRecord recorder = null;
	private boolean isRecording = false;
	
	public NoiseLevelRecorder() {
		bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,
				RECORDER_CHANNELS,RECORDER_AUDIO_ENCODING)*10;
	}
	
	public void startRecording(String filename) {
        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,RECORDER_AUDIO_ENCODING, bufferSize);

		recorder.startRecording();
		
		isRecording = true;
		
		recordingThread = new Thread(new Runnable() {

			public void run() {
				writeNoiseLevelToDatabase();
			}

		}, "AudioRecorder Thread");

		recordingThread.start();		
	}

	protected void writeNoiseLevelToDatabase() {
		byte data[] = new byte[bufferSize];

		int read = 0;

		while (isRecording) {
			read = recorder.read(data, 0, bufferSize);

			if (AudioRecord.ERROR_INVALID_OPERATION != read) {				
			}
		}
	}

	public void stopRecording() {
		// TODO Auto-generated method stub

	}

	public String getFilePath() {
		// TODO Auto-generated method stub
		return null;
	}

}